package exo1;

import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;


import java.math.*;

public class BibliUtilities {
	IGlobalBibliographyAcces gbAccess;
	private static int delaiInventaire=12;
	private Clock clock;
	
	
	
	public BibliUtilities()
	{
		super();
		gbAccess=new GlobalBibliographyAccess();
		clock=Clock.systemDefaultZone();
	}
	
	public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException
	{
		NoticeStatus res=null;
		try
		{
			NoticeBibliographique nb=gbAccess.getNoticeFromIsbn(isbn);
			res=Bibliothèque.getInstance().addNotice(nb);
		}
		catch (IncorrectIsbnException e)
		{
			throw new AjoutImpossibleException();
		}
		return res;
	}
	
	public ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref)
	{
		ArrayList<NoticeBibliographique> LectureCo = new ArrayList();
		LectureCo=gbAccess.noticesDuMemeAuteurQue(ref);
		
		ArrayList<NoticeBibliographique> Renvois = new ArrayList();
		boolean flag;
		for (NoticeBibliographique elt:LectureCo)
		{
			if (elt.getTitre()!=ref.getTitre() && Renvois.size()<=5)
			{
				flag=true;
				for (NoticeBibliographique element:Renvois)
				{
					if (element.getTitre()==elt.getTitre())
					{
						flag=false;
					}
				}
				if(flag)
				{
					Renvois.add(elt);
				}
			}
		}
		return Renvois;
	}
	
	public boolean prevoirInventaire()
	{
		return Math.abs(LocalDate.now(clock).until(Bibliothèque.getInstance().getLastInventaire()).toTotalMonths())>=delaiInventaire;
		//return true;
	}
	
	
	
}
