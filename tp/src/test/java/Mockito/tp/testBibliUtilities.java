package Mockito.tp;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.Mockito;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import exo1.BibliUtilities;
import exo1.Bibliothèque;

//@ExtendWith(MockitoExtension.class)
public class testBibliUtilities {
	
	@Mock 
	Clock mockedClock;
	//Clock mockedClock=mock(Clock.class);
	
	@InjectMocks 
	BibliUtilities biblio=new BibliUtilities();
	//BibliUtilities biblio=mock(BibliUtilities.class);
	
	@Test
	void TestDate()
	{
		//given
		LocalDate uneDate = LocalDate.of(2021, 12, 01);
		Clock fixedClock = Clock.fixed(uneDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		
		
		Bibliothèque.getInstance().inventaire();
		
		//when-then
		assertFalse(biblio.prevoirInventaire());
	}
	
	@Test
	void TestDatePour364J()
	{
		//given
		LocalDate uneDate = LocalDate.of(2021, 12,01 );
		LocalDate plustard=uneDate.plusDays(364);
		Clock fixedClock = Clock.fixed(plustard.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
				
		
		Bibliothèque.getInstance().inventaire();
		
		//when-then
		assertFalse(biblio.prevoirInventaire());
	}
	
	@Test
	void TestDatePour365J()
	{
		//given
		LocalDate uneDate = LocalDate.of(2021, 12,01 );
		LocalDate plustard=uneDate.plusDays(365);
		Clock fixedClock = Clock.fixed(plustard.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
						
		//when
		Bibliothèque.getInstance().inventaire();
		
		//then
		assertFalse(biblio.prevoirInventaire());
	}
	
	@Test
	void TestDatePour366J()
	{
		//given
		Clock clock=Clock.systemDefaultZone();
		LocalDate uneDate = LocalDate.of(2021, 12,01 );
		LocalDate plustard=uneDate.plusDays(366);
		//System.out.println(plustard);
		Clock fixedClock = Clock.fixed(plustard.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		//System.out.println(LocalDate.now(clock));					
		//when
		Bibliothèque.getInstance().inventaire();
		
		//then
		assertTrue(biblio.prevoirInventaire());
		
	}
	
	@Test
	void TestDate13M()
	{
		//given
		LocalDate now = LocalDate.now();
		LocalDate dans13mois=now.plusMonths(13);
		Clock fixedClock = Clock.fixed(dans13mois.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();					
		//when
		Bibliothèque.getInstance().inventaire();
		System.out.println(fixedClock);
		
		//then
		assertTrue(biblio.prevoirInventaire());
		
	}
}



